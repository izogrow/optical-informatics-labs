a =5;
N = 512;   % количество узлов сетки
s =1;
M = 4096; % количество узлов сетки после дополнения. Поддерживаются только четные M и N
                          % M должно быть больше N.
% Создаем Гауссов пучек и сетку для него:
h_x = 2*a/(N-1);
x = (-a:h_x: a);
Gaussian_beam = exp(-s * x.^2);

plot_abs(x, Gaussian_beam, "Гауссов пучок")
plot_phs(x, Gaussian_beam, "Гауссов пучок")

% Реализовываем одномерное финитное преобразование Фурье с помощью 
% применения алгоритма ПФ методом прямоугольников:

[ksi_reduced, F_reduced] = rectangle_finit_ft(Gaussian_beam, M, N, a);

plot_abs(ksi_reduced, F_reduced, strcat("ПФ -s * x^2 методом прямоугольников"))
plot_phs(ksi_reduced, F_reduced, strcat("ПФ -s * x^2 методом прямоугольников"))

% Сравним результат со встроенной функцией fft()
[ksi_reduced, F_2_reduced] = finite_fft(Gaussian_beam, M, N, a);

plot_abs(ksi_reduced, F_reduced, "БПФ -s * x^2")
plot_phs(ksi_reduced, F_reduced, "БПФ -s * x^2")

% Строим на одном графике:

plot(ksi_reduced, abs(F_reduced)); hold on;
plot(ksi_reduced, abs(F_2_reduced)); 
legend("ПФ методом прямоугольников","БПФ"); hold off;
title("Амплитуда");
xlabel("Частота")
ylabel("Усиление")
plot(ksi_reduced, angle(F_reduced)); hold on;
plot(ksi_reduced, angle(F_2_reduced)); 
legend("ПФ методом прямоугольников", "БПФ"); hold off;
title("Фаза");
xlabel("Частота")
ylabel("Сдвиг фазы")

% Подаем на вход алгоритма другую функцию
f = 2*x.*exp(-(x.^2)/2);

plot_abs(x, f, "2*x*exp(-x^2/2)")
plot_phs(x, f, "2*x*exp(-x^2/2)")

[ksi_reduced, F_reduced] = rectangle_finit_ft(f, M, N, a);

plot_abs(ksi_reduced, F_reduced, "ПФ 2*x*exp(-x^2/2) методом прямоугольников");
plot_phs(ksi_reduced, F_reduced, "ПФ 2*x*exp(-x^2/2) методом прямоугольников");

% Встроенная функция быстрого преобразования Фурье fft():

[ksi_reduced, F_2_reduced] = finite_fft(f, M, N, a);

plot_abs(ksi_reduced, F_2_reduced, "БПФ 2*x*exp(-x^2/2)");
plot_phs(ksi_reduced, F_2_reduced, "БПФ 2*x*exp(-x^2/2)");

% Аналитическое преобразование Фурье:

F_analytical = -4*sqrt(2) * 1i* pi^(3/2) * ksi_reduced.* exp(-2*pi^2*ksi_reduced.^2);

plot_abs(ksi_reduced, F_analytical, "Аналитического преобразования Фурье 2*x*exp(-x^2/2)")
plot_phs(ksi_reduced, F_analytical, "Аналитического преобразования Фурье 2*x*exp(-x^2/2)")

% Строим на одном графике:

plot(ksi_reduced, abs(F_reduced)); hold on;
plot(ksi_reduced, abs(F_2_reduced));
plot(ksi_reduced, abs(F_analytical));
legend("ПФ методом прямоугольников", "Быстрое преобразование Фурье", "Аналитическое преобразование Фурье"); hold off;
title("Амплитуда 2*x*exp(-x^2/2)");
xlabel("Частота")
ylabel("Усиление")
plot(ksi_reduced, angle(F_reduced)); hold on;
plot(ksi_reduced, angle(F_2_reduced)); 
plot(ksi_reduced, angle(F_analytical));
legend("ПФ методом прямоугольников", "Быстрое преобразование Фурье", "Аналитическое преобразование Фурье"); hold off;
title("Фаза 2*x*exp(-x^2/2)");
xlabel("Частота")
ylabel("Сдвиг фазы")

% Далее рассмотрим двумерный случай. Построим гауссов пучок: 

p =1;
x = (-a:h_x: a);
y = (-a:h_x: a);
[X, Y] = meshgrid(x, y);
Gaussian_beam_2d = exp(-s * X.^2 - p * Y.^2);
surf(X, Y, abs(Gaussian_beam_2d), 'EdgeColor', 'none');
title("Амплитуда Гауссова пучка");
xlabel("Частота x")
ylabel("Частота y")
zlabel("Усиление")
surf(X, Y, angle(Gaussian_beam_2d), 'EdgeColor', 'none');
title("Фаза Гауссова пучка");
xlabel("Частота x")
ylabel("Частота y")
zlabel("Сдвиг фазы")

% Затем сделаем его БПФ:
F = finite_fft_2d(Gaussian_beam_2d, M, N, a);
[ksix, ksiy] = meshgrid(ksi_reduced, ksi_reduced);
plot_2d_abs(ksix, ksiy, F, "-s * X^2 - p * Y^2")
plot_2d_phs(ksix, ksiy, F, "-s * X^2 - p * Y^2")

view([-62.13 47.49])

%Подадим на вход функцию из варианта:
x = (-a:h_x: a);
y = (-a:h_x: a);
[X, Y] = meshgrid(x, y);
f_2d = 2*X.*exp(-(X.^2)/2)*2.*Y.*exp(-(Y.^2)/2);
plot_2d_abs(ksix, ksiy, f_2d, "2*X*exp(-(X.^2)/2)*2*Y.*exp(-(Y.^2)/2)")
plot_2d_phs(ksix, ksiy, f_2d, "2*X*exp(-(X.^2)/2)*2*Y.*exp(-(Y.^2)/2)")

F = finite_fft_2d(f_2d, M, N, a);
[ksix, ksiy] = meshgrid(ksi_reduced, ksi_reduced);
plot_2d_abs(ksix, ksiy, F, "БПФ 2*X*exp(-(X.^2)/2)*2*Y.*exp(-(Y.^2)/2)")
plot_2d_phs(ksix, ksiy, F, "БПФ 2*X*exp(-(X.^2)/2)*2*Y.*exp(-(Y.^2)/2)")

view([-69.29 28.25])

%Построим аналитическое решение:
F_2d_analytical = -32 * pi^3 * ksix.*ksiy.* exp(-2*pi^2*ksix.^2-2*pi^2*ksiy.^2 );
plot_2d_abs(ksix, ksiy, F_2d_analytical, "Аналитика")
plot_2d_phs(ksix, ksiy, F_2d_analytical, "Аналитика")

view([0.74 90.00])

%Тест:
i = (1:10);
k = (21:30)';
answer = k * i;
F = zeros([N,N]);
Gaussian_beam_2d(1,:)


%Реализация моего финитного преобразования Фурье:
function F = rectangle_ft(f, M, h_x_b)
    iter = (0:M-1);
    F = zeros([1,M]);
    for k=0:M-1
        F(k+1) = sum(f.* exp((-2*pi*1i*k*iter)/M)) * h_x_b; % считаем преобразование Фурье
    end
end

function plot_abs(x, f, str_legend)
    plot(x, abs(f)); hold on;
    legend(str_legend); hold off;
    title("Амплитуда")
    xlabel("Частота")
    ylabel("Усиление")
end

function plot_phs(x, f, str_legend)
    plot(x, angle(f)); hold on; 
    legend(str_legend); hold off;
    title("Фаза")
    xlabel("Частота")
    ylabel("Сдвиг фазы")
end

function plot_2d_abs(x, y, f, str_legend)
    surf(x, y, abs(f), 'EdgeColor', 'none'); hold on;
    colorbar; legend(str_legend); hold off;
    title("Амплитуда")
    xlabel("Частота x")
    ylabel("Частота y")
    zlabel("Усиление")
end

function plot_2d_phs(x, y, f, str_legend)
    surf(x, y, angle(f), 'EdgeColor', 'none'); hold on;
    colorbar; legend(str_legend); hold off;
    title("Фаза")
    xlabel("Частота x")
    ylabel("Частота y")
    zlabel("Сдвиг фазы")
end

function [ksi_reduced, F_reduced] = rectangle_finit_ft(f, M, N, a)
    r = (M - N) / 2; % кол-во элементов, добовляемых перед и после f
    f_extended = [zeros([1, r]), f, zeros([1, r])];
    %h_x_extended = 2*a/(M-1)


    b = N / (4 * a);
    h_x_b = 2*b/(M-1);
    % меняем местами первую и вторую половину f для финитного преобразования
    f_replaced = [f_extended((length(f_extended)/2)+1:length(f_extended)),f_extended(1:length(f_extended)/2)];

    F = rectangle_ft(f_replaced, M, h_x_b); % реализация функции в конце файла

    % обратно меняем местами первую и вторую часть F
    F = [F((length(F)/2)+1:length(F)),F(1:length(F)/2)];

    b_reduced = (N^2)/(4*a*M);
    F_reduced = F(r+1:length(F)-r);

    %ksi = (-b:2*b/(M-1):b)
    ksi_reduced = (-b_reduced:2*b_reduced/(N-1):b_reduced);
end

function [ksi_reduced, F_reduced] = finite_fft(f, M, N, a)
    r = (M - N) / 2; % кол-во элементов, добовляемых перед и после f
    f_extended = [zeros([1, r]), f, zeros([1, r])];
    %f_extended = [zeros([r, 1]), f, zeros([r, 1])];
    %h_x_extended = 2*a/(M-1)


    b = N / (4 * a);
    h_x_b = 2*b/(M-1);
    % меняем местами первую и вторую половину f для финитного преобразования
    f_replaced = [f_extended((length(f_extended)/2)+1:length(f_extended)),f_extended(1:length(f_extended)/2)];

    F = fft(f_replaced, M) * h_x_b;

    % обратно меняем местами первую и вторую часть F
    F = [F((length(F)/2)+1:length(F)),F(1:length(F)/2)];

    b_reduced = (N^2)/(4*a*M);
    F_reduced = F(r+1:length(F)-r);

    %ksi = (-b:2*b/(M-1):b)
    ksi_reduced = (-b_reduced:2*b_reduced/(N-1):b_reduced);
end

function F = finite_fft_2d(f, M, N, a)
    F = zeros([N,N]);
    for i=1:N
        [~, F(i, :)] = finite_fft(f(i, :), M, N, a);
    end
    
    for k=1:N
        [~, F(:, k)] = finite_fft(F(:,k)', M, N, a);
    end  
end
