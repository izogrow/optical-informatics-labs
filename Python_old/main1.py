from numpy import cos, e, absolute, arctan
import numpy as np
import matplotlib.pyplot as plt


alpha = 1
betta = 1


def core(ksi, x):
    return x * cos(alpha * ksi ** 2 * x)


def f(x):
    return e ** (complex(0, 1) * betta * x)


if __name__ == '__main__':
    plt.figure(1)
    a, b = -1, 1
    p, q = -1, 1
    n = 1000
    m = 1000
    x = np.linspace(a, b, n)
    farr = [f(x) for x in x]
    ksi = np.linspace(p, q, m)
    # x_grid, ksi_grid = np.meshgrid(x, ksi)
    # core_val = core(x_grid, ksi_grid)
    # print(core_val)
    # Амплитуда
    plt.plot(x, np.abs(farr))
    plt.title("Амплитуда")
    # Фаза
    plt.figure(2)
    plt.plot(x, np.angle(farr))
    plt.title("Фаза")


    h = (b - a) / n
    F =[]

    for l in range(m):
        f_temp = 0
        for k in range(n - 1):
            f_temp += (core(ksi[l], x[k]) * f(k) * h)
        F.append(f_temp)

    plt.figure(3)
    print(F)
    plt.plot(ksi, F)
    #
    # F = core_val.dot(farr) * h
    # plt.plot(ksi, F)
    plt.show()
