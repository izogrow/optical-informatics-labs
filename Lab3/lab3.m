%Лабораторная работа 3
%Тема: РЕАЛИЗАЦИЯ ОПТИЧЕСКОГО ПРЕОБРАЗОВАНИЯ ХАНКЕЛЯ ДЛЯ РАДИАЛЬНО-ВИХРЕВЫХ ПУЧКОВ.
%Вариант 18

%Сначала задаем константы:
R =5;
N = 512;   % количество узлов сетки
M = 4096;   % количество узлов сетки для частотного пространства БПФ
%s =1;
m = -3;

%Построим график f(r):
h_r = R / (N - 1);
r = (0:h_r:R);

f_r = besselj(5, r); %.*exp(m*1i*fi)
plot_abs(r, f_r, "f(r)")
plot_phs(r, f_r, "f(r)")

%Найдем декартовый массив:
a = find_matrix(f_r, N, m);
%a_new = find_matrix_new(f_r, N, m);

%Построим f(r, fi):
[jj_grid, kk_grid] = meshgrid(0:(2*N), 0:(2*N));
plot_2d_abs(jj_grid, kk_grid, a, "f(r, fi)");
plot_2d_phs(jj_grid, kk_grid, a, "f(r, fi)");

%Произведем преобразование Ханкеля:
F_ro = rectangle_ht(r, f_r, m, N, h_r);

%Измеряем время выполнения преобразования Ханкеля:
f = @() rectangle_ht(r, f_r, m, N, h_r);
hankel_time = timeit(f)

%Построим F(ro):
plot_abs(r, F_ro, "F(ro)")
plot_phs(r, F_ro, "F(ro)")

%Вычислим матрицу F(ro)*exp(i*m*theta):
tic
A = find_matrix(F_ro, N, m);
toc

%Построим F(ro)*exp(i*m*theta)
plot_2d_abs(jj_grid, kk_grid, A, "F(ro, theta)");
plot_2d_phs(jj_grid, kk_grid, A, "f(ro, theta)");

%Произведем преобразование Фурье от входной двумерной функции:
h_r_upscaled = R / (2*N);
n = 2*N;
a_for_fft = a(1:end-1,1:end-1);
F_fft = finite_fft_2d(a_for_fft, M, n, 0, R);

%Измеряем время выполнения БПФ:
f = @() finite_fft_2d(a_for_fft, M, n, 0, R);
fft_time = timeit(f)

%Построим f(r, fi):
[jj_fft_grid, kk_fft_grid] = meshgrid(0:(2*N-1), 0:(2*N-1));
plot_2d_abs(jj_fft_grid, kk_fft_grid, F_fft, "F(r, fi)");
plot_2d_phs(jj_fft_grid, kk_fft_grid, F_fft, "f(r, fi)");

%Функции:
function F = rectangle_ht(x, f, m, N, h_x)
    F = zeros([1, N]);
    k = (0:N-1);
    
    for iter=[k;x]
        F(iter(1)+1) = sum(f.* besselj(m, 2*pi*x*iter(2)).*x.*h_x);
  
    end
    
    F = (2*pi/(1i^m)) * F;
end

function alfa = find_matrix_new(f_r, N, m)
    %a = zeros([2*N+1, 2*N+1]);
    n = N - 1; % кол-во отрезков
    [j_grid, k_grid] = meshgrid(0:(2*N),0:(2*N));
    
    alfa = round(sqrt((j_grid - n).^2 + (k_grid - n).^2));
    f_r_matrix = f_r(alfa);
    alfa = alfa.*(alfa > n).* f_r_matrix.* exp(m*1i*atan2(j_grid-n,k_grid-n));    

end

function a = find_matrix(f_r, N, m)
    a = zeros([2*N+1, 2*N+1]);
    n = N - 1; % кол-во отрезков
    for jj = 0:(2*N)
        for kk = 0:(2*N)
            alfa = round(sqrt((jj - n)^2 + (kk - n)^2));
            if alfa > n
                a(jj+1, kk+1) = 0;
            else
                a(jj+1, kk+1) = f_r(alfa+1) * exp(m*1i*atan2(kk-n,jj-n));
            end
        end
    end
end

function [ksi_reduced, F_reduced] = finite_fft(f, M, N, a1, a2)
    r = (M - N) / 2; % кол-во элементов, добовляемых перед и после f
    f_extended = [zeros([1, r]), f, zeros([1, r])];

    b = N / (2 * (a2 - a1));
    h_x_b = 2*b/(M-1);
    
    % меняем местами первую и вторую половину f для финитного преобразования
    f_replaced = [f_extended((length(f_extended)/2)+1:length(f_extended)),f_extended(1:length(f_extended)/2)];

    F = fft(f_replaced, M) * h_x_b;

    % обратно меняем местами первую и вторую часть F
    F = [F((length(F)/2)+1:length(F)),F(1:length(F)/2)];

    %b_reduced = (N^2)/(4*a*M);
    b_reduced = (N^2)/(2*(a2 - a1)*M);
    F_reduced = F(r+1:length(F)-r);

    ksi_reduced = (-b_reduced:2*b_reduced/(N-1):b_reduced);
end

function F = finite_fft_2d(f, M, N, a, b)
    F = zeros([N,N]);
    for iter=1:N
        [~, F(iter, :)] = finite_fft(f(iter, :), M, N, a, b);
    end
    
    for k=1:N
        [~, F(:, k)] = finite_fft(F(:,k)', M, N, a, b);
    end  
end

function F = fft_2d(f, N, h_x)
    F = zeros([N,N]);
    for iter=1:N
        shifted_f = fftshift(f(iter, :));
        F(iter, :) = fftshift(fft(shifted_f, N));
    end
    
    for k=1:N
        shifted_f = fftshift(F(:,k)');
        F(:, k) = fftshift(fft(shifted_f, N));
    end  
    F = F * h_x;
end

function plot_abs(x, f, str_legend)
    plot(x, abs(f)); hold on;
    legend(str_legend); hold off;
    title("Амплитуда")
    xlabel("Частота")
    ylabel("Усиление")
end

function plot_phs(x, f, str_legend)
    plot(x, angle(f)); hold on; 
    legend(str_legend); hold off;
    title("Фаза")
    xlabel("Частота")
    ylabel("Сдвиг фазы")
end

function plot_2d_abs(x, y, f, str_legend)
    surf(x, y, abs(f), 'EdgeColor', 'none'); hold on;
    colorbar; legend(str_legend); hold off;
    title("Амплитуда")
    xlabel("Частота x")
    ylabel("Частота y")
    zlabel("Усиление")
    view([0.549 90.000])
end

function plot_2d_phs(x, y, f, str_legend)
    surf(x, y, angle(f), 'EdgeColor', 'none'); hold on;
    colorbar; legend(str_legend); hold off;
    title("Фаза")
    xlabel("Частота x")
    ylabel("Частота y")
    zlabel("Сдвиг фазы")
    view([0.549 90.000])
end
